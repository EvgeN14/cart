import { screen, render, fireEvent } from "@testing-library/react";
import Counter from ".";

describe("Counter", () => {
  test("test Counter", () => {
    render(<Counter />);
    expect(screen.findAllByText("count__down")).toBeInTheDocument;
    expect(screen.findAllByText("count__up")).toBeInTheDocument;
  });
  test("test Counter", () => {
    render(<Counter />);

    const btnDown = screen.getByTestId("testDown");
    fireEvent.click(btnDown);
    expect(screen.getByTestId("testCountInput")).toBeInTheDocument();
    const btnUp = screen.getByTestId("testUp");
    fireEvent.click(btnUp);
    expect(screen.getByTestId("testCountInput")).toBeInTheDocument();
  });
});
