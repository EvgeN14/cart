import CartFooter from "../CartFooter";
import CartHeader from "../CartHeader";
import Product from "../Product";
import dataProduct from "../../dataProduct";
import { useState } from "react";
import { useEffect } from "react";

const Cart = () => {
  const [cart, setCart] = useState(dataProduct);

  const [total, setTotal] = useState({
    price: cart.reduce((prev, curr) => prev + curr.priceTotal, 0),
    count: cart.reduce((prev, curr) => prev + curr.count, 0),
  });

  useEffect(() => {
    setTotal({
      price: cart.reduce((prev, curr) => prev + curr.priceTotal, 0),
      count: cart.reduce((prev, curr) => prev + curr.count, 0),
    });
  }, [cart]);
  const deleteproduct = (id) => {
    setCart((cart) => cart.filter((product) => id != product.id));
  };

  const increase = (id) => {
    setCart((cart) => {
      return cart.map((product) => {
        if (product.id === id) {
          const newCountPriceUp = product.count + 1;
          return {
            ...product,
            count: newCountPriceUp,
            priceTotal: newCountPriceUp * product.price,
          };
        }

        return product;
      });
    });
  };
  const decrease = (id) => {
    setCart((cart) => {
      return cart.map((product) => {
        if (product.id === id) {
          const newCountPriceDown =
            product.count - 1 > 1 ? product.count - 1 : 1;

          return {
            ...product,
            count: newCountPriceDown,
            priceTotal: newCountPriceDown * product.price,
          };
        }
        return product;
      });
    });
  };

  const changeValue = (id, value) => {
    setCart((cart) => {
      return cart.map((product) => {
        if (product.id === id) {
          return {
            ...product,
            count: value,
            priceTotal: value * product.price,
          };
        }
        return product;
      });
    });
  };
  const products = cart.map((product) => {
    return (
      <Product
        product={product}
        key={product.id}
        deleteproduct={deleteproduct}
        increase={increase}
        decrease={decrease}
        changeValue={changeValue}
      />
    );
  });
  return (
    <section className="cart">
      <CartHeader />
      {products}
      <CartFooter total={total} />
    </section>
  );
};

export default Cart;
