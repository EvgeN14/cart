import { screen, render, fireEvent } from "@testing-library/react";

import ButtonDelete from ".";


describe("delete", () => {
test("test Buttondel", () => {
  render(<ButtonDelete />);
    const btn = screen.getByRole("button");
    expect(btn).toBeInTheDocument

});

test("click event", () => {
  render(<ButtonDelete />);

    expect(screen.queryByTestId("button")).toBeNull();

});

});

