const ButtonDelete = ({deleteproduct, id}) => {
  return (
    <button type="button" onClick={()=>{deleteproduct (id)}}>
      <img src="./img/icons/cross.svg" alt="Delete" />
    </button>
  );
};

export default ButtonDelete;
