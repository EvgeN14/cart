import ButtonDelete from "../ButtonDel";
import Counter from "../Counter";
import "./style.scss";

const Product = ({
  product,
  deleteproduct,
  increase,
  decrease,
  changeValue,
}) => {
  const { img, title, count, priceTotal, id } = product;

  const priceviev = new Intl.NumberFormat();
  return (
    <section className="product">
      <div className="product__img">
        <img src={`./img/products/${img}`} alt={title} />
      </div>
      <div className="product__title">{title}</div>
      <div className="product__count">
        <Counter
          count={count}
          increase={increase}
          decrease={decrease}
          id={id}
          changeValue={changeValue}
        />
      </div>
      <div className="product__price">{priceviev.format(priceTotal)} $.</div>
      <div className="product__controls">
        <ButtonDelete deleteproduct={deleteproduct} id={id} />
      </div>
    </section>
  );
};

export default Product;
