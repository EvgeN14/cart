import "./_vars.scss";
import "./_base.scss";
import "./_reset.scss";
import "./section_cart.scss";
import TitlePage from "../TitlePage";
import Cart from "../Cart";





const App = () => {
  return (
    <section classNameName="section-cart">
      <header className="section-cart__header">
        <div className="container">
<TitlePage/>
        </div>
      </header>
      <div className="section-cart__body">
        <div className="container">
<Cart/>
        </div>
      </div>
    </section>
  );
};
export default App;
