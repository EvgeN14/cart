import "./style.scss";

const CartFooter = ({ total }) => {
  const { count, price } = total;
  const priceviev = new Intl.NumberFormat();
  return (
    <footer className="cart-footer">
      <div className="cart-footer__count">{count} единицы</div>
      <div className="cart-footer__price">{priceviev.format(price)} $.</div>
    </footer>
  );
};

export default CartFooter;
