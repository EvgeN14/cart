const dataProduct = [
  {
    id: 1,
    img: "google.jpg",
    title: "Google",
    count: 1,
    price: 200,
    priceTotal: 200,
  },
  {
    id: 2,
    img: "Tesla.jpg",
    title: "Tesla",
    count: 1,
    price: 300,
    priceTotal: 300,
  },
  {
    id: 3,
    img: "Strava.jpg",
    title: "Strava",
    count: 1,
    price: 5,
    priceTotal: 5,
  },
];
export default dataProduct;
